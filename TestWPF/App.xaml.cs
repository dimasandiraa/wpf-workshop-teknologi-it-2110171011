﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace TestWPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private KaryawanHomepage karyawanHomepage;
        private LoginPage loginPage;
        private RegisterPage registerPage;
        private TambahBarangPage tambahBarangPage;
        private TambahTransaksiPage tambahTransaksiPage;
        private TransaksiPage transaksiPage;

        public App()
        {
            loginPage = new LoginPage();
            karyawanHomepage = new KaryawanHomepage();
            registerPage = new RegisterPage();
            tambahBarangPage = new TambahBarangPage();
            tambahTransaksiPage = new TambahTransaksiPage();
            transaksiPage = new TransaksiPage();

            MainWindow = loginPage;
            loginPage.Show();
        }

        public static App GetInstance()
        {
            
            return (App)Current;
        }

        public void GoToKaryawanHomepage()
        {
            HideAllWindow();
            karyawanHomepage.ShowDialog();
        }

        public void GoToLoginPage()
        {
            HideAllWindow();
            loginPage.ShowDialog();
        }

        public void GoToRegister()
        {
            HideAllWindow();
            registerPage.ShowDialog();
        }

        public void GoToTambahBarang()
        {
            HideAllWindow();
            tambahBarangPage.ShowDialog();
        }

        public void GoToTambahTransaksi()
        {
            HideAllWindow();
            tambahTransaksiPage.ShowDialog();
        }

        public void GoToTransaksi()
        {
            HideAllWindow();
            transaksiPage.ShowDialog();
        }

        private void HideAllWindow()
        {
            loginPage.Hide();
            karyawanHomepage.Hide();
            registerPage.Hide();
            tambahBarangPage.Hide();
            tambahTransaksiPage.Hide();
            transaksiPage.Hide();
        }

        public void CloseAllWindow()
        {
            foreach(Window win in Windows)
            {
                Console.WriteLine(win.Title);
                win.Close();
            }
        }

    }
}

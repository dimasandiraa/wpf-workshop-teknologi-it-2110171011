﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TestWPF
{
    /// <summary>
    /// Interaction logic for TambahBarangPage.xaml
    /// </summary>
    public partial class TambahBarangPage : Window
    {
        public TambahBarangPage()
        {
            InitializeComponent();
        }

        private void tambah_btn_Click(object sender, RoutedEventArgs e)
        {
            App.GetInstance().GoToTransaksi();
        }

        private void batal_btn_Click(object sender, RoutedEventArgs e)
        {
            App.GetInstance().GoToTransaksi();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            App.GetInstance().CloseAllWindow();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TestWPF
{
    /// <summary>
    /// Interaction logic for KaryawanHomepage.xaml
    /// </summary>
    public partial class KaryawanHomepage : Window
    {
        public KaryawanHomepage()
        {
            InitializeComponent();
        }

        private void keluar_btn_Click(object sender, RoutedEventArgs e)
        {
            App.GetInstance().GoToLoginPage();
        }

        private void transaksiBaru_btn_Click(object sender, RoutedEventArgs e)
        {
            App.GetInstance().GoToTambahTransaksi();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            App.GetInstance().CloseAllWindow();
        }
    }
}

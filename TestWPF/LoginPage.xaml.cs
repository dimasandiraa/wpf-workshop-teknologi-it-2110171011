﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;

namespace TestWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class LoginPage : Window
    {

        public LoginPage()
        {
            InitializeComponent();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            App.GetInstance().GoToKaryawanHomepage();
        }

        private void Register_Click(object sender, RoutedEventArgs e)
        {
            App.GetInstance().GoToRegister();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            App.GetInstance().CloseAllWindow();
        }
    }
}
